
Description
===========
Alexa Widgets module 

The Alexa Widgets module provides a block which displays one of the Alexa widgets
described at http://www.alexa.com/siteowners/widgets which is either: 
* Site Info (Alexa Site Stats Button)
* Traffic Rank (Alexa Traffic Rank Button)

The block will be enabled on the http://example.com/admin/build/block page and has the title "Alexa Widgets".

Requirements
============
Drupal 7

Installation
============
1.  Copy the alexa_widgets folder to the appropriate Drupal directory (e.g. sites/all/modules).

2.  Enable the Alexa Widgets module in the "Site building -> Modules" administration screen (e.g. admin/build/modules).

3.  Enable the Alexa Widgets block (e.g. on the http://example.com/admin/build/block page)

Usage
=====
1. Go to admin/settings/alexa_widgets to define the widget.

2. Enable the Alexa Widgets block (e.g. on the http://example.com/admin/build/block page) and ensure that it is
   visible.